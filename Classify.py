from tkinter import *
from tkinter import messagebox

root = Tk()
root.geometry("250x100")

def check():
    global response
    tweets = open('tweets_1.txt', 'r')

    for tweet in tweets:
        if len(tweet) == 1:
            continue
        response = messagebox.askquestion("Check breaking", tweet)
        print(response)

        tweet = tweet[:len(tweet)-1]
        writeFile = open('tweets.csv', 'a')

        writeFile.write(tweet + ', ')

        if response == 'yes':
            writeFile.write('1\n')
        else:
            writeFile.write('0\n')

        writeFile.close()


btn = Button(root, text = 'Next', command = check)
btn.pack()
root.mainloop()
