from message import Message
import math
import numpy as np

class TfIdf:
	wordAndFrequency = {}
	posOfUniqueWord = {}
	currentPos = 0
	defaultTweets = 1
	inverseDocumentFrequency = {}
	tweetCount = 1

	@classmethod
	def increasedSize(cls, msg, outOfVocabulary):
		tweekToken = msg.tokenize()
		termFrequency = {}
		weight_vector = {} # https://www.quora.com/What-is-a-tf-idf-vector
		sizeBefore = len(cls.wordAndFrequency)

		for t in tweekToken:
			if t not in termFrequency:
				termFrequency[t] = 1
				if t not in cls.wordAndFrequency:
					cls.wordAndFrequency[t] = 1
					cls.posOfUniqueWord[t] = cls.currentPos
					cls.currentPos += 1
				else:
					cls.wordAndFrequency[t] += 1
			else:
				termFrequency[t] += 1

		cls.tweetCount += 1
		increasedSize = len(cls.wordAndFrequency) - sizeBefore

		for key, value in termFrequency.items():
			inverseDocumentFrequency_value = 0
			if key in cls.inverseDocumentFrequency:
				inverseDocumentFrequency_value = cls.inverseDocumentFrequency[key]
				cls.inverseDocumentFrequency[key] = math.log10((cls.tweetCount)/(cls.wordAndFrequency[key] + 1))
			else:
				inverseDocumentFrequency_value = math.log10(cls.tweetCount)
				cls.inverseDocumentFrequency[key] = inverseDocumentFrequency_value

			weight_vector[cls.posOfUniqueWord[key]] = value * inverseDocumentFrequency_value

		weight_vector = normalization(weight_vector)

		msg.update(weight_vector)

		return increasedSize


def euclideanNorm(dic):
	norm = 0
	for key, value in dic.items():
		norm += math.pow(value, 2)

	return math.sqrt(norm)

def normalization(dic):
	norm = euclideanNorm(dic)

	for key, value in dic.items():
		if norm == 0:
			dic[key] = 0
		else:
			dic[key] = dic[key] / norm

	return dic
