from flask import Flask, render_template
import re
import ast
from nltk import ne_chunk, pos_tag, word_tokenize
from nltk.tree import Tree

app = Flask(__name__)

def get_continuous_chunks(text):
    chunked = ne_chunk(pos_tag(word_tokenize(text)))
    prev = None
    continuous_chunk = []
    current_chunk = []

    for i in chunked:
        if type(i) == Tree:
            current_chunk.append(" ".join([token for token, pos in i.leaves()]))
        elif current_chunk:
            named_entity = " ".join(current_chunk)
            if named_entity not in continuous_chunk:
                continuous_chunk.append(named_entity)
                current_chunk = []
        else:
            continue

    if continuous_chunk:
        named_entity = " ".join(current_chunk)
        if named_entity not in continuous_chunk:
            continuous_chunk.append(named_entity)

    return continuous_chunk[:-1]


#f = open("displayNews.txt", "r")

f = open("displayNews_temp.txt", "r")

tweetText = []
urlText = []
userText = []
dateText = []
imgText = []
tag = []

def removeUnwanted(text):
	text = re.sub(r"https\S+","", text)
	text = re.sub(r'@\w+',"", text)
	text = re.sub(r'RT', "", text)
	text = re.sub(r' : ', "", text)

	return text

for l in f:
	#l = l[1:-1]
	l = ast.literal_eval(l)
	#print(l)
	#print(removeUnwanted(l[0]))
	
	#date, user, tweet, url, img = l.split(',')
	if removeUnwanted(l[0]) not in tweetText:
		tweetText.append(removeUnwanted(l[0]))
		urlText.append(l[3])
		userText.append(l[1])
		dateText.append(l[2])
		imgText.append(re.sub(r'_normal', "", l[4]))
		tag.append(get_continuous_chunks(l[0]))
	

'''
for l in f:
	print(l)
	tweetText.append(l)
'''
print(tweetText)

@app.route("/home")
def index_page():
	return render_template('home.html', tweetText=tweetText, urlText=urlText, userText=userText, dateText=dateText, imgText=imgText, tag=tag)
	
@app.route("/happenings")
def happenings_page():
	return render_template('happenings.html')

@app.route("/sports")
def sports_page():
	return render_template('sports.html')
	
@app.route("/political")
def political_page():
	return render_template('political.html')

@app.route("/entertainment")
def entertainment_page():
	return render_template('entertainment.html')

@app.route("/landing")
def landing_page():
	return render_template('landing.html')




if __name__=="__main__":
	app.debug = True
	app.run()