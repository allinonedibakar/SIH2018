'''
This script is used for Twitter Streaming API
First we are Streaming based on country location box
Then we are preprocessing the data
Storing it in JSON format in a text file 
'''

# Importing required packages
from tweepy import Stream
from tweepy import OAuthHandler
from tweepy.streaming import StreamListener
import time
import json
import tweetQueue

# OAuth
# consumer key, consumer secret, access token, access secret (key can be obtained from https://apps.twitter.com/)
ckey="SixW2Xobg1ABvO5KaULsZvByu"
csecret="XEphTRXF6kITrP7K7SupAw6gikvBaxGdlae8Baomm4fVLQIP6P"
atoken="2291009460-sROlMwXLJhEKWZrSe6jp5qrQnB3pqCXGNWatTjL"
asecret="LyuNEtW1FEueNnReAGwBsNSuWCfEUmws5MEW8rsBH5TI2"

# Store tweet in a queue which can be processed afterwards
tweetsQueue = tweetQueue.notProcessedTweets

#override tweepy.streaming.StreamListener to add logic to on_data, on_error
class listener(StreamListener):
	numOfTweetsRead = 0 # keep track of number of tweets
	tweetsData = [] # list containing tweets which can be finally write to a JSON file

	def on_data(self, data):
		# print(data)
		dic_data = json.loads(data) # data from API (JSON) to python dictionary 
		userDetails = {} # Empty for now user detail
		tweetDetails = {} # Empty for now tweet detail
		global tweetsQueue # Queue which is used to store tweets to process afterwards

		# collect all desire details
		if 'user' in dic_data:
			userDetails["name"] = dic_data["user"]["name"]
			userDetails["id"] = dic_data["user"]["id"]
			userDetails["numFollowers"] = dic_data["user"]["followers_count"]
			userDetails["numFollowing"] = dic_data["user"]["friends_count"]
			userDetails["screen_name"] = dic_data["user"]["screen_name"]
			userDetails["profilePic"] = dic_data["user"]["profile_image_url"]
			userDetails["verified"] = dic_data["user"]["verified"]
			# print(userDetails["verified"])

			tweetDetails["created_at"] = dic_data["created_at"]
			tweetDetails["id_str"] = dic_data["id_str"]
			tweetDetails["user"] = userDetails
			tweetDetails["tweetText"] = dic_data["text"]
			tweetDetails["id"] = dic_data["id"]
			tweetDetails["hashtags"] = dic_data["entities"]["hashtags"]
			tweetDetails["tagUsers"] = dic_data["entities"]["user_mentions"]
			tweetDetails["timestamp"] = dic_data["timestamp_ms"]
			tweetDetails["coordinates"] = dic_data["coordinates"]
			tweetDetails["link"] = dic_data["entities"]["urls"]

			self.numOfTweetsRead = self.numOfTweetsRead + 1

			# print(tweetDetails)
			self.tweetsData.append(tweetDetails)
			tweetsQueue.append(tweetDetails) # append tweet to Queue

			# write to the file at every 60 tweets
			if self.numOfTweetsRead % 60 == 0:
				with open("tweets.json", "a") as f:
					for tweet in self.tweetsData:
						f.write(json.dumps(tweet) + ',\n')

				self.tweetsData = []

			# display the user some feedback when 20 tweets are recieved
			if self.numOfTweetsRead % 50 == 0:
				print("Read", str(self.numOfTweetsRead), "tweets...")

		return True

	# on error
	def on_error(self, status):
		print("Error:", status)

		# Response code: 420
		# Text: Enhance Your Calm
		# Cause: Returned when an application is being rate limited for making too many requests.
		if status == 420:
			time.sleep(5)
			return True

	def on_exception(self, exception):
               print(exception)
               return

def stream():
	auth = OAuthHandler(ckey, csecret)
	auth.set_access_token(atoken, asecret)
	twitterStream = Stream(auth, listener())

	# Source for location
	# https://gist.github.com/graydon/11198540
	# http://boundingbox.klokantech.com/ [choose CSV, bottom left]
	# https://en.wikipedia.org/wiki/User:The_Anome/country_bounding_boxes
	# twitterStream.filter(locations=[68.1766451354, 7.96553477623, 97.4025614766, 35.4940095078], async=True)
	# twitterStream.filter(locations=[60.5,-7.5,125.9,38.5], async=True)
	twitterStream.filter(track=["news", "breaking", "latest"], async=True)



'''Extra code which is commented

print('tweet' + data)

if len(re.findall(r'full_text', data)) > 0:
	text = data.split('"full_text":"')[1].split('","')[0]
else:
	text = data.split(',"text":"')[1].split('","')[0]
text = re.sub(r"https\S+","", text)
text = re.sub(r"\\n","", text)
text = re.sub(r'@\w+',"", text)
text = re.sub(r'\\u[0-9a-f]{4}', '', text)
text = re.sub(r'RT', "", text)
text = re.sub(r'[^\w\s]', ' ', text)
print(text)
file_handler = open('tweets_1.txt', 'a')
file_handler.write(text + "\n\n")
print()

# print(userDetails)
# print()
# print(tweetDetails)
# print()
'''