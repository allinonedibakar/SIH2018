import message
from collections import deque
import random

class Bucket:
	hashData = {}

	def __init__(self, size):
		self.size = size

	def insertion(self, tweet, entity):
		if entity not in self.hashData:
			self.hashData[entity] = deque([], maxlen=self.size)

		self.hashData[entity].append(tweet)

	def collision(self, entity):
		if entity not in self.hashData:
			return []
		else:
			return list(self.hashData[entity])

	def retrieval(self, entity):
		return entity in self.hashData

class Database(object):

	randomVectorForBucket = []
	ListOfBuckets = []

	def __init__(self, width = 0, numOfHashTable = 0, size = 0):
		self.width = width
		self.numOfHashTable = numOfHashTable
		self.size = size

		randomVector = [[] for line in range(self.width)]

		for i in range(self.numOfHashTable):
			Database.randomVectorForBucket.append(randomVector)
			Database.ListOfBuckets.append(Bucket(self.size))

	
	def increaseBucketSize(self, size):
		if size <= 0:
			return
			
		for bck in range(self.numOfHashTable):
			for x in range(self.width):
				lst = Database.randomVectorForBucket[bck][x]
				for i in range(size):
					lst.append(random.gauss(0, 1))


	def nearestNeighbour(self, msg):
		randomBucketCount = 0
		numberOfNeighbour = []

		for bck in Database.ListOfBuckets:
			vector = msg.returnVector()
			hashVal = 0

			for x in range(self.width):
				#totalProduct = 0
				#for key, value in vector.items():
				#	totalProduct = totalProduct + value*Database.randomVectorForBucket[randomBucketCount][x][key]
				totalProduct = sum([value*Database.randomVectorForBucket[randomBucketCount][x][key] for key, value in vector.items()])
				if totalProduct >= 0:
					hashVal = hashVal | (1<<x)

			randomBucketCount += 1
			numberOfNeighbour += bck.collision(hashVal)
			bck.insertion(hashVal, msg)

		return numberOfNeighbour