import json
import collections
from multiprocessing import Queue
import numpy as np
import time
import tweetQueue
import re

#queue = collections.deque()
completelyChecked = tweetQueue.processedTweets

lowerLimitForEntropy = 3
lowerLimitForUnique = 2
discardNewsIfGreater = 3
lowerTweetInCurrentSession = 1
lowerLimitForCossim = .3
sessionTime = 30

potentialNews = {}
potentialNewsMap = {}
potentialNewsUserList = {}
displayNewsList = []

def start():
	print("collecting tweets...")
	begin = 0
	tweetTimestamp = 0
	numberOfProcessed = 0


	while True:
		if completelyChecked.empty():
			print("No tweets in unprocessed queue. Waiting to fill up...")
			time.sleep(2)
		else:
			#print("Tweet Present")
			newTweet = completelyChecked.get()
			if newTweet['cossim'] < lowerLimitForCossim:
				addNews(newTweet)
			else:
				addToExisting(newTweet)

			tweetTimestamp = int(newTweet['timestamp']) / 1000
			# print(tweetTimestamp - begin)
			if tweetTimestamp - begin > sessionTime:
				data = createFormatData(begin, tweetTimestamp)
				with open("newsTweets.json", "a") as f:
					if data != None:
						f.write(json.dumps(data) + '\n')
						dataDic = json.loads(data)

						print(type(dataDic))

						for k, v in dataDic.items():
							if k not in ['begin', 'end']:
								for k2, v2 in v.items():
									#print(v2['header'])

									if len(v2['header'][4]) > 0:
										displayNewsList.append([v2['header'][1].replace(",", ";"), v2['header'][2], v2['header'][3], v2['header'][4][0]['url'], v2['header'][6]])
									else:
										displayNewsList.append([v2['header'][1].replace(",", ";"), v2['header'][2], v2['header'][3], 'http://twitter.com/' + v2['header'][2] + '/status/' + str(v2['header'][5]), v2['header'][6]])
						writeNews()


				print("Susccessfully write.")
				begin = tweetTimestamp
				# return tweets to flask
				#print(data)

def writeNews():
	f = open("displayNews_temp.txt", "w")
	f.close()
	with open("displayNews_temp.txt", "w") as f:
		for l in displayNewsList:
			f.write(str(l) + "\n")

def tokenize(message):
	token = ' '.join(re.sub("(@[A-Za-z0-9_.]+)|([^0-9A-Za-z \t])|(\w+:\/\/\S+)"," ", message.lower()).split()).split()
	# print(tokens)
	return token

def calculateEntropy(news):
	#print(news['header'][1])
	tokens = tokenize(news['header'][1])
	
	for i in [tweet[1] for tweet in news['tweets']]:
		tokens += tokenize(i)
	length = len(tokens)
	dic = {}
	for token in tokens:
		if token not in dic:
			dic[token] = 0
		else:
			dic[token] += 1
	A = np.fromiter(dic.values(), dtype=float)
	pA = A / A.sum()
	return -np.sum(pA*np.log2(pA))



def createFormatData(begin, tweetTimestamp):
	newDic = {}
	newDic['header'] = {}

	for key, value in potentialNews.items():

		if value:
			#print(value)
			if value['timesAppear'] < 1:
				value['dropChance'] += 1
			else:
				value['dropChance'] = 0
				entropy = calculateEntropy(value)
				uniquiness = value['unique']

				if entropy >= lowerLimitForEntropy and uniquiness >= lowerLimitForUnique:
					newDic['header'][key] = value

	newDic['begin'] = begin
	newDic['end'] = tweetTimestamp

	newsTweets = None

	if len(newDic['header']) > 0:
		newsTweets = json.dumps(newDic)
		#print(newsTweets)

	for key, value in potentialNews.items():
		if value:
			value['timesAppear'] = 0
			value['tweets'] = []

			if value['dropChance'] >= discardNewsIfGreater:
				potentialNews[key] = None

	return newsTweets

def addNews(msg):
	newsData = {}
	if 'Sat Mar 31' in msg['created_at']:
		potentialNewsUserList[msg['id']] = set([msg['user']['id']])

		newsData['unique'] = 1
		newsData['dropChance'] = 0
		newsData['timesAppear'] = 1
		newsData['total'] = 1

		numberOfEntities = 0
		# numberOfUserMention = 0
		if 'entities' in msg:
			numberOfEntities = len(msg['entities'])
		#if 'user_mentions' in msg:
			# numberOfUserMention = len(msg['user_mentions'])

		data = (msg['id'], msg['tweetText'], msg['user']['screen_name'], msg['created_at'], msg['link'], msg["id_str"], msg['user']['profilePic'], numberOfEntities)
		
		newsData['header'] = data
		newsData['tweets'] = [data]

		potentialNews[msg['id']] = newsData
		potentialNewsMap[msg['id']] = msg['id']

def updateNews(msg, mid):
	newsData = {}
	potentialNewsUserList[mid] = set([msg['user']['id']])

	newsData['unique'] = 1
	newsData['dropChance'] = 0
	newsData['timesAppear'] = 1
	newsData['total'] = 1

	numberOfEntities = 0
	# numberOfUserMention = 0
	if 'entities' in msg:
			numberOfEntities = len(msg['entities'])
	#if 'hashtags' in msg:
	#	numberOfHashTag = len(msg['hashtags'])
	#if 'user_mentions' in msg:
	#	numberOfUserMention = len(msg['user_mentions'])

	data = (msg['id'], msg['tweetText'], numberOfEntities)
	
	newsData['header'] = data
	newsData['tweets'] = [data]

	potentialNews[mid] = newsData
	potentialNewsMap[msg['id']] = mid

def addToExisting(msg):
	#print(potentialNewsMap)
	#print("Nearest", msg['nearestNeighbour'])
	mid = potentialNewsMap[msg['nearestNeighbour']]

	if potentialNews[mid]:
		numberOfHashTag = 0
		#numberOfUserMention = 0
		userList = potentialNewsUserList[mid]
		userList.add(msg['user']['id'])

		if 'entities' in msg:
			numberOfHashTag = len(msg['entities'])
		#if 'user_mentions' in msg:
		#	numberOfUserMention = len(msg['user_mentions'])
		data = (msg['id'], msg['tweetText'], numberOfHashTag)

		oldNews = potentialNews[mid]
		oldNews['total'] += 1
		oldNews['timesAppear'] += 1
		oldNews['unique'] = len(userList)
		oldNews['tweets'].append(data)

		potentialNews[mid] = oldNews
		potentialNewsMap[msg['id']] = mid
	else:
		updateNews(msg, mid)
