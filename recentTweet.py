import math

def cosineSimilarity(value1, value2):
	dotProd = 0.0
	for k,v in value1.items():
		if k in value2:
			dotProd += value1[k]*value2[k]

	norm = euclideanNorm(value1) * euclideanNorm(value2)
	cossim = 0.0
	if norm != 0:
		cossim = dotProd / norm

	return cossim

def euclideanNorm(dic):
	norm = 0
	for key, value in dic.items():
		norm += math.pow(value, 2)

	return math.sqrt(norm)

class RecentTweets:
	recentTweets = []
	def __init__(self, size):
		self.size = size

	def nearestNeighbour(self,msg):
		if len(RecentTweets.recentTweets) == 0:
			return (None,-2.0)

		v1 = msg.returnVector()
		closest = (None,-2.0)

		for t in RecentTweets.recentTweets:
			#print(t)
			v2 = t.returnVector()
			sim = cosineSimilarity(v1,v2)
			if  sim > closest[1]:
				closest = (t,sim)

		return closest

	def insertion(self,msg):
		if len(RecentTweets.recentTweets)==self.size:
			RecentTweets.recentTweets = RecentTweets.recentTweets[1:]

		RecentTweets.recentTweets.append(msg)
