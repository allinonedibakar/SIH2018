## Project for SIH2018

**Ministry Category** : Ministry of Information and Broadcasting

**Problem Statement** : Identify potential breaking news based on social media chatter (twitter,
Facebook etc.)

**Problem Code** : #MIB10

**Team Code** : 12118

**Team Name** : BAKA_CODERS

### Milestone

- [x] Stream tweets (using tweepy)

- [x] tdidf (implementation of TFIDF)

- [x] Message (contain require functionality for tfidf, such as tokening and creating tfidf vector)

- [x] implemented bucket object for LSH

- [x] extractNews (main file for news categorization)

- [x] run (enty point of program)

- [x] NER (using NER give every tweets some relevant tags)

- [ ] Detection of fake news and find the original/real news (implementation in progress)

### How to run the program:

First of all you need to install all the modules needed for the program. Also you need anaconda as here one virtual environment is created. List of modules can be found in **requirement.txt**.

Alternatively you can use:

```
**while read requirement; do conda install --yes $requirement; done < requirements.txt**
```

After installing all the required modules the backend program can be run by executing **run.bat**.

After that the dashboard can be lauched by executing **runDashboard.bat**.

To open the dashboard go to browser and visit *http://127.0.0.1:5000/landing*.

From here we can go to *Services* and then *Potential Breaking New* to enter the dashboard where we are displaying the potential breaking news to our end user.


