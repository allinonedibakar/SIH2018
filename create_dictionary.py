import os
import numpy as np

word_list = []
lst=[]
seen = set()
coded_stream = np.zeros(100)
word_count = 100

def sizer(arr, n):
    out = np.zeros(n)
    out[:len(arr)] = arr
    return out

for i in range(1,2):
    tweet_handler = open('tweet_{}.csv'.format(i), 'r')
    for line in tweet_handler:
        sentence =  line.split(',')
        #print(sentence[0])
        words = sentence[0].split(' ')
        for word in words:
            if word not in seen:
                seen.add(word)
                word_list.append(word)

# print(len(word_list))
print('read all tweets\n')
print(word_list)
word_dictionary = {word: i for word,i in zip(word_list, range(1, len(word_list)+1 ))}

dictionary_handler = open('word_dictionary.txt', 'w')
for data,i in word_dictionary.items():
    dictionary_handler.write(data + ' : ' + str(i) + '\n')

print('created dictionary for all words\n')
# print(word_dictionary)

print('started encoding , may take a while\n ')
for i in range(1,2):
    file_handler = open('tweet_{}.csv'.format(i), 'r')
    for line in file_handler:
        sentence = line.split(',')
        #print(sentence[0])
        words = sentence[0].split(' ')
        temp_list = []
        for word in words:
            temp_list.append(word_dictionary[word])
        temp_array = np.zeros(word_count)
        temp_array[:len(temp_list)] = np.array(temp_list)
        lst.append(temp_array)
        print(lst)
        #print(temp_list)
        '''
        if len(temp_list) > 0:
            temp_array = np.zeros(word_count)
            temp_array[:len(temp_list)] = np.array(temp_list)
            #print(temp_array)
            
            #print(coded_stream)
            #print('#', end='')
        '''
        coded_stream = np.array(lst)

#np.vstack(sizer(np.array(i), max(map(len, lst))) for i in lst if i)

print('\nencoding tweets done !')
print(coded_stream)
np.save('encoded_tweets.npy',coded_stream)