# Importing required packages
from tweepy import Stream
from tweepy import OAuthHandler
from tweepy.streaming import StreamListener
import time
import json
import tweetQueue

# OAuth
# consumer key, consumer secret, access token, access secret.
# Shubham
ckey="4cIpg1jQnZcy2biRFgYcx7QNI"
csecret="5plxi4BC7JQGiDuzdbtuPeRmPAtnJ5aSNNQn8kk942EgQdhSPU"
atoken="876816658798706688-xEmuwJJQQwthHlHiGfLeAfK2i18lYe2"
asecret="vyjBN8z1HVYA0V9dts7FoFY3Kw76ExankbdXP49orrF6U"


fakeNews = "PM Modi mass address to Indian diaspora in London cancelled due to a planned huge agitation by Sikhs"

def tokenize(message):
	token = ' '.join(re.sub("(@[^A-Za-z0-9_.]+)|([^0-9A-Za-z \t])|(\w+:\/\/\S+)"," ", message.lower()).split()).split()
	# print(tokens)
	return token

class listener(StreamListener):
	numOfTweetsRead = 0 # keep track of number of tweets
	tweetsData = [] # list containing tweets which can be finally write to a JSON file

	def on_data(self, data):
		# print(data)
		dic_data = json.loads(data) # data from API (JSON) to python dictionary 
		userDetails = {} # Empty for now user detail
		tweetDetails = {} # Empty for now tweet detail
		global tweetsQueue # Queue which is used to store tweets to process afterwards

		# collect all desire details
		if 'user' in dic_data:
			userDetails["name"] = dic_data["user"]["name"]
			userDetails["id"] = dic_data["user"]["id"]
			userDetails["numFollowers"] = dic_data["user"]["followers_count"]
			userDetails["numFollowing"] = dic_data["user"]["friends_count"]

			tweetDetails["created_at"] = dic_data["created_at"]
			tweetDetails["user"] = userDetails
			tweetDetails["tweetText"] = dic_data["text"]
			tweetDetails["id"] = dic_data["id"]
			tweetDetails["hashtags"] = dic_data["entities"]["hashtags"]
			tweetDetails["tagUsers"] = dic_data["entities"]["user_mentions"]
			tweetDetails["timestamp"] = dic_data["timestamp_ms"]
			tweetDetails["coordinates"] = dic_data["coordinates"]

			self.numOfTweetsRead = self.numOfTweetsRead + 1

			# print(tweetDetails)
			self.tweetsData.append(tweetDetails)
			# tweetsQueue.append(tweetDetails) # append tweet to Queue

			# write to the file at every 60 tweets
			if self.numOfTweetsRead % 60 == 0:
				with open("allNews.json", "a") as f:
					for tweet in self.tweetsData:
						f.write(json.dumps(tweet) + ',\n')

				self.tweetsData = []

			# display the user some feedback when 20 tweets are recieved
			if self.numOfTweetsRead % 50 == 0:
				print("Read", str(self.numOfTweetsRead), "tweets...")

		return True

def stream(fakeNews):
	auth = OAuthHandler(ckey, csecret)
	auth.set_access_token(atoken, asecret)
	twitterStream = Stream(auth, listener())

	fakeNewsToken = [fakeNews]
	# Source for location
	# https://gist.github.com/graydon/11198540
	# http://boundingbox.klokantech.com/ [choose CSV, bottom left]
	# https://en.wikipedia.org/wiki/User:The_Anome/country_bounding_boxes
	# twitterStream.filter(locations=[68.1766451354, 7.96553477623, 97.4025614766, 35.4940095078], async=True)
	# twitterStream.filter(locations=[60.5,-7.5,125.9,38.5], async=True)
	twitterStream.filter(track=fakeNews, async=True)

stream(fakeNews)