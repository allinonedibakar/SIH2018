'''
The main file which use LSH algorithm
'''

# import require package
import get_stream
import collections
from multiprocessing import Queue
from bucket import Bucket, Database
from recentTweet import RecentTweets
from datetime import datetime
import threading
import extractNews
import tweetQueue
from message import Message
from tfidf import TfIdf
import sys
from operator import itemgetter

# Queue to keep track of processed and unprocessed tweets
totalUnprocessedQueue = tweetQueue.notProcessedTweets
totalProcessedQueue = tweetQueue.processedTweets

# Start Streaming
print("Fetching tweets...")
get_stream.stream()

# Constrainst
WIDTH = 13
NUMBEROFHASHTABLE = 36
QUEUESIZE = 30
RECENTTWEETS = 1000
MINTOKEN = 2
IGNORE_KEY = ['im', 'me', 'mine']
SEARCH_TAG = ['#news', '#breakingnews']

# 
bucket = Database(WIDTH, NUMBEROFHASHTABLE, QUEUESIZE)
recent = RecentTweets(RECENTTWEETS)

def euclideanNorm(dic):
	norm = 0
	for key, value in dic.items():
		norm += math.pow(value, 2)

	return math.sqrt(norm)

def cosineSimilarity(value1, value2):
	dotProd = 0.0
	for k,v in value1.items():
		if k in value2:
			dotProd += value1[k]*value2[k]

	norm = euclideanNorm(value1) * euclideanNorm(value2)
	cossim = 0.0
	if norm != 0:
		cossim = dotProd / norm

	print("cossim", cossim)

	return cossim

def calculateClosestCossim(msg, neigbour):
    bestT = None
    bestC = -2.0
    for n in neigbour:
        if msg != n:
            c = cosineSimilarity(msg.returnVector(), n.returnVector())
            if c > bestC:
                bestT = n
                bestC = c

    return (bestT,bestC)

def closestNeighboursFromBuckets(msg):
	#print("Entered")
	possibleBucket = bucket.nearestNeighbour(msg)
	aggregatingDuplicates = {}
	look = {}
	
	for x in possibleBucket:
		if x.mid in aggregatingDuplicates:
			aggregatingDuplicates[x.mid] += 1
		else:
			aggregatingDuplicates[x.mid] = 1
			look[x.mid] = x

	neigbour = sorted(iter(aggregatingDuplicates.items()), key=itemgetter(1), reverse=True)
	neigbour = [z[0] for z in neigbour[:min(len(neigbour), 3*NUMBEROFHASHTABLE)]]
	neigbour = [look[k] for k in neigbour]
	#print("inside 1")
	return calculateClosestCossim(msg, neigbour)

def closestNeighboursFromRecentTweets(msg, closestBucket):
	closest = (None, -2)
	if((1-closestBucket) >= 0.5):
		#print("inside 2")
		closest = recent.nearestNeighbour(msg)
	recent.insertion(msg)
	#print(msg)
	return closest

def checkClosest(buckets, recentNews):
	if buckets[1] > recentNews[1]:
		return buckets
	else:
		return recentNews

def is_ascii(mystring):
	return all(ord(c) < 128 for c in mystring)

def valid(msg, taglist, ignoreKey, minimumToken):
	t = None
	if is_ascii(msg):
		t = Message(msg,0,0,0)
	else:
		#print("Not Valid ASCII")
		return False
	tokens = t.tokenize()
	#print(t)
	#print("token:",tokens)
	if len(tokens) >= minimumToken and not any(w in tokens for w in ignoreKey):
		return True
	else:
		return False

numberOfTweetsProcessed = 0
begin = 0
systime = datetime.now()
msg = None
failed = 0
passed = 0
totalTweet = 0
oovMap= {}
'''
with open(sys.argv[1]) as f:
    for l in f:
        ws        = l.split()
        k         = str(ws[0])
        v         = str(ws[1])
        oovMap[k] = v
'''
trd = threading.Thread(target=extractNews.start)
trd.start()

while True:
	if len(totalUnprocessedQueue) != 0:
		t	 = totalUnprocessedQueue.popleft()
		msg   = t['tweetText']
		ts	= int(t['timestamp'])
		mid = int(t['id'])
		uid   = int(t['user']['id'])
		#print("mgid",mid, "uid", uid)
		#print("t:", t)
		#print("Checking Valid")
		if valid(msg, SEARCH_TAG, IGNORE_KEY, MINTOKEN):
			#print("Valid Pass")
			tweet = Message(msg, ts, mid, uid)
			#print(tweet)
			increment = TfIdf.increasedSize(tweet, oovMap)
			bucket.increaseBucketSize(increment)
			closestBucket	= closestNeighboursFromBuckets(tweet)
			#print("clse bucket", closestBucket)
			closestRecentTweets  = closestNeighboursFromRecentTweets(tweet, closestBucket[1])
			#print("clse recent", closestRecentTweets[1])
			overallClosest = checkClosest(closestBucket,closestRecentTweets)
			#print("closest", overallClosest[0])
			other = overallClosest[0]

			#print(other)

			if other:
				t['nearestNeighbour'] = other.mid
			else:
				t['nearestNeighbour'] = -1
			t['cossim'] = overallClosest[1]
			totalProcessedQueue.put(t)
		else:
			#print('Didn\'t Pass the Validation')
			failed += 1


		totalTweet += 1
		current = int(ts) / 1000
		if current-begin > 900:
			aftertime = datetime.now()
			delta	 = aftertime-systime
			systime   = aftertime
			dt		= divmod(delta.seconds,60)
			print('Tweets done in ' + str(dt[0]) + ' min ' + str(dt[1]) + ' sec.\n')
			begin = current

		numberOfTweetsProcessed += 1
		if numberOfTweetsProcessed%100 == 0:
			print('Proccesed tweet count:', numberOfTweetsProcessed)


