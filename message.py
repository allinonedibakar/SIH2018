import re

class Message:
	def __init__(self, message, timestamp, mid, uid):
		self.message = message
		self.mid = mid
		self.uid = uid
		self.timestamp = timestamp
		self.hashtags = None
		self.token = []
		self.vector = None

	def __str__(self):
		return str(self.timestamp) + ':' + self.message

	def update(self, dic):
		self.vector = dic

	def returnVector(self):
		if len(self.vector) > 0:
			return self.vector
		else:
			return {}

	def returnHashTags(self):
		return re.findall(r"#(\w+)", message)
			
	def tokenize(self):
		self.token = ' '.join(re.sub("(@[A-Za-z0-9_.]+)|([^0-9A-Za-z \t])|(\w+:\/\/\S+)"," ", self.message.lower()).split()).split()
		# print(tokens)
		return self.token

